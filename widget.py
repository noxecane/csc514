from kivy.app import App
from kivy.uix.image import Image
from graphviz import Graph
from app import graph, utils, traverse, dls as algo

IMG_PATH = 'imgs/graph.zip'


class MyPaintApp(App):

    def __init__(self):
        super().__init__()
        self.graph_path = Image(anim_delay=1)
        self.graph_path.source = IMG_PATH

    def build(self):
        return self.graph_path

if __name__ == '__main__':
    g = Graph('mygraph', format='png')
    g2 = []
    g2_edge = graph.edgek(g2)

    # create model edges
    g2_edge('A', 'B')
    g2_edge('A', 'C')
    g2_edge('B', 'D')
    g2_edge('C', 'E')
    g2_edge('C', 'F')

    # draw to graphviz path
    traverse.draw_graphk(g2, g)

    # create adjacency matrix
    m = graph.adjacency_matrix(g2)
    algo.traverse_graph(m, traverse.traverse_graph(g), 'A', 1)

    #utils.zippath('imgs/', IMG_PATH)
    MyPaintApp().run()
