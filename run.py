from app import bfs, dfs, dls, ids, traverse as t


graph = [
    [0, 1, 1, 0, 0, 0, 0],
    [0, 0, 0, 1, 1, 0, 0],
    [0, 0, 0, 0, 0, 1, 1],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
]


bfspath = []
bfs.traverse_graph(graph, t.iso_trace(bfspath), 'a')

dfspath = []
dfs.traverse_graph(graph, t.iso_trace(dfspath), 'a')


dlspath = []
dls.traverse_graph(graph, t.iso_trace(dlspath), 'a', 1)

idspath = []
ids.traverse_graph(graph, t.iso_trace(idspath), 'a')

print('BFS Path', '->'.join(bfspath))
print('DFS Path', '->'.join(dfspath))
print('DLS Path', '->'.join(dlspath))
print('IDS Path', '->'.join(idspath))
