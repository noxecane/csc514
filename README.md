# CSC514 Term Project

We will be working on:
+ Uninformed Search Algorithms
+ Heuristic Search Algorithms
+ Artificial Neural Networks
+ Genetic Algorithms

Check out:
+ [Search](http://www.tutorialspoint.com/artificial_intelligence/index.htm)
+ [ANN](https://www.youtube.com/watch?v=bxe2T-V8XRs&list=PL77aoaxdgEVDrHoFOMKTjDdsa0p9iVtsR)
+ [Genetic](https://www.youtube.com/watch?v=zwYV11a__HQ)