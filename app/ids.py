from app import graph, lib

index = graph.offset('A')


def traverse_graph(g, fn, label):
    visited = set()
    s = []
    maxdepth = 1

    def traverse(node, depth):
        nonlocal maxdepth, s
        visited.add(node)
        if fn(node):
            return node

        if depth == maxdepth and len(s) != 0:
            return traverse(lib.stack_popk(s), depth)

        if depth == maxdepth and len(s) == 0:
            maxdepth += 1

        if depth < maxdepth:
            unodes = graph.unvisited(g, visited, node)
            s.extend(unodes)
            if len(s) != 0:
                return traverse(lib.stack_popk(s), depth)

    return traverse(index(label), 0)
