from app import graph, lib

index = graph.offset('A')


def traverse_graph(g, fn, label, maxdepth):
    visited = set()
    s = []

    def traverse(node, depth):
        visited.add(node)
        if fn(node):
            return node
        unodes = graph.unvisited(g, visited, node)
        s.extend(unodes)

        if len(s) != 0 and depth < maxdepth:
            depth = depth - 1 if len(unodes) == 0 else depth + 1
            return traverse(lib.stack_popk(s), depth)
    return traverse(index(label), 0)
