from app import lib
from app.lib import compose, curry


@curry
def offset(l1, l2):
    '''
    Gets the offset of a label from another
    @sig offset :: Str -> Str -> Int
    '''
    return ord(l2) - ord(l1)


@curry
def label(s, o):
    return chr(s + o)


def index_edge_set(edgeset):
    '''
    Creates a mapping of the edge labels to integers
    according to the order of the edges.
    @sig index_edge_set :: [Str] -> {Str: Int}
    '''
    topoffset = offset(edgeset[0])
    return {k: topoffset(k) for k in edgeset}


@curry
def index_edge_tuples(iedgeset, edgetuples):
    '''
    Uses an indexed edge set to replace the labels
    in the edge tuples with indices
    @sig index_edge_tuples :: {Str: Int} -> (Str, Str) -> (Int, Int)
    '''
    return [(iedgeset[a], iedgeset[b]) for a, b in edgetuples]


@curry
def empty_matrix(row, col, default):
    '''
    Creates an empty 2-dimensional array with default value
    in all items in each row.
    @sig empty_matrix :: Int -> Int -> a
    '''
    return [[default] * col for _ in range(row)]


@curry
def edgek(edgetuples, a, b):
    '''
    Adds an edge tuple to a list of other edge tuples.
    The tuple must consist character labels.
    @sig edgek :: [(Str, Str)] -> Str -> Str -> [(Str, Str)]
    '''
    edgetuples.append((a, b))
    return edgetuples


def edge_set(tuples):
    '''
    Returns a set of the edges of a graph from the
    given edge tuples.
    @sig edges :: (Str, Str) -> {Str}
    '''
    return __edges(tuples)
__edges = compose(set, lib.flatten)


@curry
def flip_edgek(matrix, edgetuple, val):
    '''
    Uses the given value to update the matrix reflecting
    the positions of the edge
    @sig flip_edgek :: [[Int]] -> (Int, Int) -> Int -> [[Int]]
    '''
    matrix[edgetuple[0]][edgetuple[1]] = val
    return matrix


def adjacency_matrix(tuples):
    '''
    Creates an adjacency matrix from a list of edge
    tuples. This only works with nodes that use lower
    case strings as labels.
    @sig adjacency_matrix :: (Str, Str) -> [[Int]]
    '''
    iedgeset = __indexed_edge_set(tuples)
    iedgetuples = index_edge_tuples(iedgeset, tuples)
    emptymatrix = empty_matrix(len(iedgeset), len(iedgeset), 0)
    for et in iedgetuples:
        flip_edgek(emptymatrix, et, 1)
    return emptymatrix

__sorted_edge_set = compose(sorted, edge_set)
__indexed_edge_set = compose(index_edge_set, __sorted_edge_set)


def adjacent(graph, index):
    '''
    Returns a list of all nodes connected to the given node
    @sig adjacent :: [[Int]] -> Int -> [Int]
    '''
    adj = []
    for i in range(len(graph[index])):
        if graph[index][i] == 1:
            adj.append(i)
    return adj


def unvisited(graph, visited, index):
    '''
    Removes the visited nodes from the list of adjacent nodes
    to the given index
    @sig unvisited :: [[Int]] -> {Int} -> Int
    '''
    return list(set(adjacent(graph, index)) - visited)
