from functools import reduce
import os
import zipfile
from zipfile import ZipFile


def ffilter(fn, ls):
    '''
    Wraps the result of filter function in a list
    @sig ffilter :: (a -> Bool) -> [a] -> [a]
    '''
    return list(filter(fn, ls))


def filter_index(fn, ls):
    '''
    Returns the indices of every element that passes the
    given predicate
    @sig filter_index :: (a -> Bool) -> [a] -> [Int]
    '''
    return list(filter(lambda i: fn(ls[i]), range(len(ls))))


def fmap(fn, ls):
    '''
    Wraps the result of map function in a list
    @sig ffilter :: (a -> b) -> [a] -> [b]
    '''
    return list(map(fn, ls))


def fnot(fn):
    '''
    Creates a function inverses the result of a given function
    @sig fnot :: (* -> Bool) -> (* -> Bool)
    '''
    return lambda *a, **b: not(fn(*a, **b))


def sforall(fn, ls):
    '''
    Runs the given function over all the values in the list
    @sig sforall :: (a -> _) -> [a] -> _
    '''
    for i in ls:
        fn(i)


def uniqify(ls):
    '''
    Removes duplicates from a list
    @sig uniqify :: [a] -> [a]
    '''
    return list(set(ls))


def first(fn, ls):
    '''
    Finds the first instance of an element of the iterable that passes
    the given predicate.
    @sig first :: (a -> Bool) -> [a] -> a
    '''
    for i in ls:
        if fn(i):
            return i


def compose(*fns):
    '''
    Performs right-to-left function composition. The rightmost function may have
    any arity, the remaining functions must be unary.
    @sig compose :: (b -> c)...(* -> b) -> (* -> c)
    '''
    return reduce(lambda f, g:
                  lambda *args: f(g(*args)), fns)


def zipdir(path, ziph):
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))


def zippath(path, f):
    zipf = ZipFile(f, 'w', zipfile.ZIP_DEFLATED)
    zipdir(path, zipf)
    zipf.close()
