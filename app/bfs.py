from app import graph, lib, utils


index = graph.offset('A')


def traverse_graph(g, fn, label):
    visited = set()
    q = []

    def traverse(adjacent):
        end = utils.first(lambda x: visited.add(x) or fn(x), adjacent)
        if end:
            return end
        q.extend(adjacent)
        unodes = graph.unvisited(g, visited, lib.queue_popk(q))
        while len(unodes) == 0 and len(q) != 0:
            unodes = graph.unvisited(g, visited, lib.queue_popk(q))

        if len(unodes) != 0:
            return traverse(unodes)
    return traverse([index(label)])
