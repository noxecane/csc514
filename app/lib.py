import functools


def curry(f):
    '''
    Returns the curried equivalent of the given function.
    @sig curry :: * -> b -> * -> b
    '''
    @functools.wraps(f)
    def c(*args, **kwargs):
        if len(args) + len(kwargs) >= f.__code__.co_argcount:
            return f(*args, **kwargs)
        return lambda *args2, **kwargs2: c(*(args + args2), **dict(kwargs, **kwargs2))
    return c


def compose(*fns):
    '''
    Performs right-to-left function composition. The rightmost function may have
    any arity, the remaining functions must be unary.
    @sig compose :: (b -> c)...(* -> b) -> (* -> c)
    '''
    return functools.reduce(lambda f, g:
                            lambda *args: f(g(*args)), fns)


@curry
def bind(fn, name, obj):
    '''
    Binds an object to function pretty much the way classes
    do.
    @sig bind :: (* -> a) -> Str -> a -> a
    '''
    setattr(obj, name, fn)
    return obj


def flatten(ls):
    '''
    Reduces a two-dimensional array to one dimension
    consisting of all elements in the second dimension.
    @sig flatten :: [[a]] -> [a]
    '''
    return [x for l in ls for x in l]


def queue_pushk(ls, val):
    '''
    Adds to the end of a queue
    @sig queue_pushk :: [a] -> a -> [a]
    '''
    ls.append(val)
    return ls


def queue_popk(ls):
    '''
    Pops the first element off the queue
    @sig queue_popk :: [a] -> a
    '''
    return ls.pop(0)


def queue_peek(ls):
    '''
    Returns the value of the head of a queue
    @sig queue_peek :: [a] -> a
    '''
    return ls[0]


def stack_pushk(ls, val):
    '''
    Adds to the end of a stack
    @sig stack_pushk :: [a] -> a -> [a]
    '''
    return ls.append(val)


def stack_popk(ls):
    '''
    Pops the last element off the stack
    @sig stack_popk :: [a] -> a
    '''
    return ls.pop()


def stack_peek(ls):
    '''
    Returns the value of the bottom of a stack
    @sig stack_peek :: [a] -> a
    '''
    return ls[len(ls) - 1]

