from app import graph, lib

index = graph.offset('A')


def traverse_graph(g, fn, label):
    visited = set()
    s = []

    def traverse(node):
        visited.add(node)
        if fn(node):
            return node
        unodes = graph.unvisited(g, visited, node)
        s.extend(unodes)
        if len(s) != 0:
            return traverse(lib.stack_popk(s))
    return traverse(index(label))
