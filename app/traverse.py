from app import graph

label = graph.label(65)

def trace(path):
    def p(x):
        path.append(graph.label(x))
        return x
    return p


def iso_trace(path):
    def p(x):
        path.append(graph.label(x))
    return p


def search(x):
    def p(y):
        return graph.label(y) == x
    return p


def draw_graphk(edgetups, g):
    for a, b in edgetups:
        g.edge(a, b)
    return g


def traverse_graph(g):
    c = 1

    def p(x):
        nonlocal c
        g.node(label(x), color='red')
        g.render('imgs/%d' % c)
        c += 1
    return p
